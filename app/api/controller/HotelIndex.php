<?php
namespace app\api\controller;

use think\facade\Db;

class HotelIndex extends Super
{

    /*
     * app 首页面
     * */
    public function index(){
        if(request()->isPost()) {
            //查询手机活动宣传图
            $list['propaganda_img'] = Db::table('app_activity_propaganda_img')->select();

//            $list['propaganda_img'] = json_encode($list['propaganda_img']);
            //手机活动顶部公告
            $list['top_notice'] = Db::table('app_activity_top_notice')->select();
            //查询所有酒店
            $map = [
                ['building_id','<>',''],
            ];
            $list['hotel_system'] = Db::table('hotel_system')->where($map)->select();
            $list['seho'] = Db::table('hotel_system')->where($map)->paginate(5);
            //处理设施
            $list['tag'] = [];
            for ($x=0; $x<count($list['hotel_system']); $x++) {
                $tag = explode(",", $list['hotel_system'][$x]['facilities']);
                array_push($list['tag'],$tag);
            }
            //房间最低价
            $list['layout_price'] = [];
            for ($x=0; $x<count($list['hotel_system']); $x++) {
                $room =  Db::table('layout')->where('building_id',$list['hotel_system'][$x]['building_id'])->min('price');
                array_push($list['layout_price'],$room);
            }
            return json([
                'msg' => $list,
                'code' => '200',
                'mycity' => "上海"
            ]);
        }
    }

    /*
     * 搜索酒店
     * */
    public function search_hotel(){

        if(request()->isPost()){
            $city = input('city');
            $map = [
                ['building_id','<>',''],
                ['city','=',$city]
            ];
            $list['hotel_system'] = Db::table('hotel_system')->where($map)->select();
            //处理设施
            $list['tag'] = [];
            for ($x=0; $x<count($list['hotel_system']); $x++) {
                $tag = explode(",", $list['hotel_system'][$x]['facilities']);
                array_push($list['tag'],$tag);
            }
            //房间最低价
            $list['layout_price'] = [];
            for ($x=0; $x<count($list['hotel_system']); $x++) {
                $room =  Db::table('layout')->where('building_id',$list['hotel_system'][$x]['building_id'])->min('price');
                array_push($list['layout_price'],$room);
            }
            return json([
                'msg' => $list,
                'code' => '200'
            ]);
        }
    }

    /*
     * 酒店详情
     * */
    public function hotel_desc(){
        if(request()->isPost()){
            if(!is_array($this->check_token(input('token')))){
                return $this->return_json('令牌错误','0');
            }
            $building_id = input('building_id');
//            $building_id = 56;
            //轮播图
            $list['img'] = Db::table('hotel_img')->where('building_id',$building_id)->select();

            $list['hotel_system'] = Db::table('hotel_system')->where('building_id',$building_id)->find();
            //处理设施
            $list['tag'] = [];
            $tag = explode(",", $list['hotel_system']['facilities']);
            array_push($list['tag'],$tag);

            //房型
            $list['layout'] = Db::table('layout')->where('building_id',$building_id)->select();

            return json([
                'msg' => $list,
                'code' => '200'
            ]);
        }

    }

    /*
     * 查询房型
     * */
    public function layouts(){
        if(request()->isPost()){
            if(!is_array($this->check_token(input('token')))){
                return $this->return_json('令牌错误','0');
            }
            //房型
            $list = Db::table('layout')->where('id',input('id'))->find();

            return json([
                'msg' => $list,
                'code' => '200'
            ]);
        }
    }

    /*
     * app支付
     * */
    public function hotel_pay(){
        $this->alipay_app();
    }

    /*
     * app订单
     * */
    public function orders(){
        if(request()->isPost()){
            $data  = input('param.');
            if(!is_array($this->check_token(input('token')))){
                return $this->return_json('令牌错误','0');
            }
            $user = Db::table('app_member')->where('token',input('token'))->find();
            $data['user_id'] = $user['id'];
            if($data['types'] == "subscribe"){
                $data['create_time'] = time();
                $res = Db::table('app_subscribe_order')->insert($data);
                if($res){
                    return $this->return_json('支付成功','200');
                }else{
                    return $this->return_json('支付失败','0');
                }
            }
        }
    }
}
