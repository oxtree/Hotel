<?php
namespace app\api\controller;

use app\BaseController;
use think\facade\Db;

class AppUser extends Super
{

    /*
     * 查询个人信息
     * */
    public function get_user(){
        if(request()->isPost()){
            $this->check_token(input('token'));
            $list = Db::table('member')->where('id',input('user_id'))->find();
            return $this->return_json($list,'200');
        }
    }

    /*
     * 添加常用用户
     * */
    public function add_user(){
        if(request()->isPost()){
            $this->check_token(input('token'));
            $list = Db::table('app_user')->insert(input('param.'));
            if($list){
                return $this->return_json('添加成功','200');
            }else{
                return $this->return_json('添加失败','0');
            }
        }
    }

    /*
     * 查询所有常用人
     * */
    public function get_alluser(){
        if(request()->isPost()){
            $this->check_token(input('token'));
            $list = Db::table('app_user')->where('user_id',input('user_id'))->select();
            return $this->return_json($list,'200');
        }
    }

    /*
     * 删除常用人
     * */
    public function del_user(){
        if(request()->isPost()){
            $this->check_token(input('token'));
            $list = Db::table('app_user')->where('id',input('id'))->delete();
            if($list){
                return $this->return_json('删除成功','200');
            }else{
                return $this->return_json('删除失败','0');
            }
        }
    }

    /*
     * 修改密码
     * */
    public function upd_pwd(){
        if(request()->isPost()){
            $this->check_token(input('token'));
            $list = Db::table('member')->where('id',input('user_id'))->find();
            if(md5(input('old_pwd')) === $list['password']){
                $res = Db::table('member')->where('id',$list['id'])->update(['password'=>md5(input('new_pwd'))]);
                if($res){
                    return $this->return_json('修改成功','200');
                }else{
                    return $this->return_json('修改失败','0');
                }
            }else{
                return $this->return_json('原密码错误','0');
            }
            dump($list);

        }
    }


}
