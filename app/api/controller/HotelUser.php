<?php
namespace app\api\controller;

use think\facade\Db;

class HotelUser extends Super
{

    /*
     * app 首页面
     * */
    public function index(){
        if(request()->isPost()) {
            if(!is_array($this->check_token(input('token')))){
                return $this->return_json('令牌错误','0');
            }
            $user = Db::table('app_member')->where('token',input('token'))->find();
            return json([
                'msg' => $user,
                'code' => '200'
            ]);
        }
    }

    /*
     * 修改密码
     * */
    public function upd_pwd(){
        if(request()->isPost()){
            if(!is_array($this->check_token(input('token')))){
                return $this->return_json('令牌错误','0');
            }
            $data = input('param.');
            $user = Db::table('app_member')->where('id',$data['id'])->find();
            if($user['password'] == md5($data['old_pwd'])){
                if(!empty($data['password']) && !empty($data['new_pwd'])){
                    if($data['password'] == $data['new_pwd']){
                        $res = Db::table('app_member')->where('id',$data['id'])->update(['password'=>md5($data['new_pwd'])]);
                        if($res){
                            return $this->return_json('修改成功','100');
                        }else{
                            return $this->return_json('修改失败','0');
                        }

                    }else{
                        return $this->return_json('俩次密码不一致','0');
                    }
                }else{
                    return $this->return_json('请输入新密码','0');
                }

            }else{
                return $this->return_json('原密码错误','0');
            }
        }
    }



}
