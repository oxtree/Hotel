<?php
namespace app\home\controller;

use app\index\controller\Basics;
use think\facade\Db;

/*
 * 预订信息
 *
 * */

class Subscribe extends Basics
{

    /*
     * 显示页面
     *
     * */
    public function index()
    {
        $data = input('param.');
//        dump($data);
        if(empty($data['start'])){
            $tiems = date("Ymd") + 1;
            $tiems = date("Y-m-d",strtotime('+1day'));
            $time = '';
        }else{
            $tiems = str_replace('-','',$data['start']);
            $time = $data['start'];
        }

        if(empty($data['type_id'])){
            $map = [
                ['a.building_id','=',session('building_id')],
            ];
        }else{
            $map = [
                ['a.building_id','=',session('building_id')],
                ['a.type_id','=',$data['type_id']]
            ];
        }
        $week = $this->count_weekss($this->diffBetweenTwoDays(date("Y-m-d"),$tiems));

        $list =  Db::table('room')
            ->alias('a')
            ->field('a.*,b.type_name,b.price,b.deposit,c.building,d.storey')
            ->join('layout b','a.type_id = b.id')
            ->join('building c','a.building_id = c.id')
            ->join('storey d','a.storey_id = d.id')
            ->where($map)
            ->paginate(['list_rows'=> 15,'query' => input('param.')]);

        $layout = $this->select_all('layout');
        $subscribe = $this->select_all('home_room_subscribe');
        return view('index',['week'=>$week['week'],'subscribe'=>$subscribe,'date'=>$week['time'],'list'=>$list,'layout'=>$layout,'time'=>$time]);
    }


    /*
     * 添加预订日期
     * */
    public function subscribe_date()
    {
        $data = input('param.');

        if(request()->isAjax()){
            $data = input('param.');
            $data['in_time'] = str_replace('-','',$data['in_time']);
            $data['move_time'] = str_replace('-','',$data['move_time']);
            $data['create_time'] = time();
            $data['operator'] = session('admin_id');
            $data['building_id'] = session('building_id');

            if($data['move_time'] - $data['in_time'] < '1' ){
                return $this->return_json('日期不对','0');
            }
            $map[] = ['in_time','like',$data['in_time']];
            $map[] = ['room_id','=',$data['room_id']];
            $rooms = Db::table('home_room_subscribe')->where($map)->find();
            if($rooms){
                return $this->return_json('该日期已经被预订','0');
            }

            $this->record($data);

            if(Db::table('home_room_subscribe')->insert($data)){

/*                Db::table('room')->where('id',$data['room_id'])->update([
                    'subscribe_in_time'=>$data['in_time'],
                    'subscribe_move_time'=>$data['move_time']
                ]);*/
                return $this->return_json('预订成功','100');
            }else{
                return $this->return_json('预订失败','0');
            }
        }
        $data = input('param.');
        $time = date("Y-m-d",strtotime($data['date']));

        $list =Db::table('room')
                ->alias('a')
                ->field('a.*,b.type_name,b.price,b.deposit,c.building,d.storey')
                ->join('layout b','a.type_id = b.id')
                ->join('building c','a.building_id = c.id')
                ->join('storey d','a.storey_id = d.id')
                ->where('a.id',$data['room_id'])
                ->find();
        return view('subscribe_date',['list'=>$list,'room_id'=>$data['room_id'],'time'=>$time]);
    }


    /*
     * 计算一周后星期几
     * */
    public function count_weekss($type = null,$max = 7){
        date("l"); //data就可以获取英文的星期比如Sunday
        date("w"); //这个可以获取数字星期比如123，注意0是星期日

        $weekarray=array("日","一","二","三","四","五","六"); //先定义一个数组
        $arr = [
            'week' =>[],
            'time'=>[]
        ];
        for($x=0; $x<$max; $x++){
            if($type == null){
                array_push($arr['week'], date("Y-m-d",strtotime("{$x} day"))."-星期".$weekarray[date("w",strtotime("{$x} day"))]);
                array_push($arr['time'],date("Ymd",strtotime("{$x} day")));
            }else{
                $num= $type+ $x;
                array_push($arr['week'], date("Y-m-d",strtotime("{$num} day"))."-星期".$weekarray[date("w",strtotime("{$num} day"))]);
                array_push($arr['time'],date("Ymd",strtotime("{$num} day")));
            }
        }
        return $arr;
    }

    /*
     * 计算价格
     * */
    public function record($data){
        $list =Db::table('room')
            ->alias('a')
            ->field('a.*,b.type_name,b.price,b.deposit,c.building,d.storey')
            ->join('layout b','a.type_id = b.id')
            ->join('building c','a.building_id = c.id')
            ->join('storey d','a.storey_id = d.id')
            ->where('a.id',$data['room_id'])
            ->find();
//        $day = $data['move_time'] - $data['in_time'];
        $day = $this->diffBetweenTwoDays($data['in_time'],$data['move_time']);

        if($day  == '0'){
            $datas['income_details'] = $list['price'];
        }else{
            $datas['income_details'] = $list['price'] * $day;
        }
//        $datas['deposit_record'] = $list['deposit'];
        $datas['deposit_record'] = '0';
        $datas['types'] = '1';
        $datas['status'] = '2';
        $datas['room_id'] = $data['room_id'];
        $datas['create_time'] = time();
        $datas['stay_days'] = $day;
        $datas['in_time'] = strtotime($data['in_time']);
        $datas['move_time'] = strtotime($data['move_time']);
        $datas['operator'] = session('admin_id');
        $datas['building_id'] = session('building_id');
        Db::table('home_subscribe_msg')->insert($datas);
    }

}
