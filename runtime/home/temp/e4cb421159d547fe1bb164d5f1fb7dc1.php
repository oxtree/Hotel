<?php /*a:3:{s:53:"D:\phpstudy_pro\WWW\tp\view\home\subscribe\index.html";i:1603758296;s:51:"D:\phpstudy_pro\WWW\tp\view\home\common\static.html";i:1603931011;s:54:"D:\phpstudy_pro\WWW\tp\view\home\common\resources.html";i:1603609812;}*/ ?>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
    <meta charset="UTF-8">
    <title><?php echo htmlentities($system['hotel_name']); ?>(多酒店版)</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="stylesheet" href="/static/admin/css/font.css">
    <link rel="stylesheet" href="/static/admin/css/xadmin.css">
    <script src="/static/admin/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="/static/admin/js/xadmin.js"></script>

    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/2.0.3/jquery.js"></script>
    <script src="/static/jquery.printarea.js"></script>

    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


    <link href="/static/toastr/toastr.css" rel="stylesheet"/>
    <script src="/static/toastr/toastr.js"></script>

</head>
<input type="hidden" value="<?php echo htmlentities($voice['types']); ?>" id="voice">

<script>
    //语音播报
    function voice(name) {
        //判断语音是否开启
        if(<?php echo htmlentities($voice['status']); ?> === '0'){
            return false;
        }
        if($('#voice').val() === '思悦'){
            var audio= new Audio("/static/voice/siyue/"+name+".mp3");
        }else if($('#voice').val() === '若兮'){
            var audio= new Audio("/static/voice/ruoxi/"+name+".mp3");
        }else if($('#voice').val() === '艾琪'){
            var audio= new Audio("/static/voice/aiqi/"+name+".mp3");
        }else if($('#voice').val() === '艾美'){
            var audio= new Audio("/static/voice/aimei/"+name+".mp3");
        }else if($('#voice').val() === '艾悦'){
            var audio= new Audio("/static/voice/aiyue/"+name+".mp3");
        }else if($('#voice').val() === '青青'){
            var audio= new Audio("/static/voice/qingqing/"+name+".mp3");
        }else if($('#voice').val() === '翠姐'){
            var audio= new Audio("/static/voice/cuijie/"+name+".mp3");
        }else if($('#voice').val() === '姗姗'){
            var audio= new Audio("/static/voice/shanshan/"+name+".mp3");
        }else if($('#voice').val() === '小玥'){
            var audio= new Audio("/static/voice/xiaoyue/"+name+".mp3");
        }
        audio.play();//播放
    }
</script>
<!--<link href="https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/3.4.0/css/bootstrap.css" rel="stylesheet">-->
<!--<link rel="stylesheet" href="/static/bootstrap/css/bootstrap.css">-->
<link href="/static/bootstrap3.0.css" rel="stylesheet" type="text/css"/>
<script src="/static/bootstrap/js/bootstrap.js"></script>

    <body>
        <div class="x-nav">
            <span class="layui-breadcrumb">
                <a href="">首页</a>
                <a>
                    <cite>预订信息</cite>
                </a>
            </span>
            <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
                <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i>
            </a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15" id="app">
                <div class="layui-col-md12">
                    <div class="layui-card">

                        <div class="layui-card-body ">
                            <form class="layui-form layui-col-space5" method="post" action="">
                                <div class="layui-input-inline layui-show-xs-block">
                                    <input class="layui-input" placeholder="开始日" name="start" id="start"></div>
                                <div class="layui-input-inline layui-show-xs-block">
                                    <select name="type_id">
                                        <option value="">全部</option>
                                        <?php if(is_array($layout) || $layout instanceof \think\Collection || $layout instanceof \think\Paginator): $i = 0; $__LIST__ = $layout;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$lay): $mod = ($i % 2 );++$i;?>
                                            <option value="<?php echo htmlentities($lay['id']); ?>"><?php echo htmlentities($lay['type_name']); ?></option>
                                        <?php endforeach; endif; else: echo "" ;endif; ?>
                                    </select>
                                </div>
                                <div class="layui-input-inline layui-show-xs-block">
                                    <button class="layui-btn" lay-submit="" lay-filter="sreach">
                                        <i class="layui-icon">&#xe615;</i></button>
                                </div>
                            </form>
                        </div>

                        <div class="layui-card-body ">
                            <table class="layui-table layui-form">
                                <thead>
                                    <tr>
                                        <th>房型</th>
                                        <th>房间号</th>
                                        <?php if(is_array($week) || $week instanceof \think\Collection || $week instanceof \think\Paginator): $i = 0; $__LIST__ = $week;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$w): $mod = ($i % 2 );++$i;?>
                                        <th><?php echo htmlentities($w); ?></th>
                                        <?php endforeach; endif; else: echo "" ;endif; ?>
                                </thead>
                                <tbody>
                                <?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                                    <tr>
                                        <td><?php echo htmlentities($vo['type_name']); ?></td>
                                        <td><?php echo htmlentities($vo['room_num']); ?></td>
                                        <?php if(is_array($date) || $date instanceof \think\Collection || $date instanceof \think\Paginator): $i = 0; $__LIST__ = $date;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$d): $mod = ($i % 2 );++$i;?>
                                        <td @click="action(<?php echo htmlentities($vo['id']); ?>,<?php echo htmlentities($d); ?>)" id="<?php echo htmlentities($vo['id']); ?>str<?php echo htmlentities($d); ?>">
                                            <?php if(is_array($subscribe) || $subscribe instanceof \think\Collection || $subscribe instanceof \think\Paginator): $i = 0; $__LIST__ = $subscribe;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v1): $mod = ($i % 2 );++$i;if($v1['room_id'] == $vo['id']): ?>
<!--                                                    预订:<?php echo htmlentities($v1['in_time']); ?><br>-->
<!--                                                    日期：<?php echo htmlentities($d); ?>-->
                                                    <?php if($v1['in_time'] <= $d && $v1['move_time'] >= $d): ?>
                                                        <button type="button"	class="layui-btn layui-btn-warm" >
                                                            已被预订
                                                        </button>
                                                        <input type="hidden" value="已被预订" id="yuding<?php echo htmlentities($vo['id']); ?><?php echo htmlentities($d); ?>">
                                                    <?php endif; ?>
                                                <?php endif; ?>

                                            <?php endforeach; endif; else: echo "" ;endif; ?>

                                        </td>
                                        <?php endforeach; endif; else: echo "" ;endif; ?>
                                    </tr>
                                <?php endforeach; endif; else: echo "" ;endif; ?>
                                </tbody>
                            </table>
                            <?php echo $list; ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </body>

    <?php if($time == ''): ?>
    <script>
        layui.use(['laydate', 'form'],
            function() {
                var laydate = layui.laydate;

                //执行一个laydate实例
                laydate.render({
                    elem: '#start' //指定元素
                    ,value: new Date(new Date().getTime() + 24*60*60*1000)
                });
            });
    </script>
    <?php else: ?>
    <script>
        var time = "<?php echo htmlentities($time); ?>";
        layui.use(['laydate', 'form'],
            function() {
                var laydate = layui.laydate;

                //执行一个laydate实例
                laydate.render({
                    elem: '#start' //指定元素
                    ,value: time
                });
            });
    </script>
    <?php endif; ?>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script>
    new 	Vue({
        el:'#app',
        data:{
        },
        methods:{
            subscribe:function(id,date){
               console.log('日期'+date+'/房间id'+id);
                $.ajax({
                    type:"post",
                    url: "<?php echo url('home/subscribe/subscribe_date'); ?>",
                    data: {
                        id:id,
                        date:date
                    },
                    success: function(data){
                        console.log(data);

                    }});
            },
            action:function (id,week) {

                if($('#yuding'+id+week).val() == '已被预订'){
                    return false;
                }
                $('#ids'+id+week).remove();
                var text = "                                             <div id=\"ids"+id+week+"\">\n" +
                        "                                                <button type=\"button\" \tclass=\"layui-btn layui-btn-warm\"\n" +
                        "                                                        onclick=\"xadmin.open('随客管理','/home/subscribe/subscribe_date/room_id/"+id+"/date/"+week+"',1000,700)\">\n" +
                        "                                                    预订\n" +
                        "                                                </button>\n" +
                        "\n" +
                        "                                                <button type=\"button\" \tclass=\"layui-btn layui-btn-danger\">锁房</button>\n" +
                        "                                            </div> ";
                $('#'+id+'str'+week).append(text);
            }
        },
        mounted:function(){
            console.log('-----4.挂载之后-----');

        },
    })

</script>




</html>